// Initialize Firebase
var config = {
    apiKey: "AIzaSyBYEYfsNWlUncHw_Mo8tNJu8nPDKGdxTrM",
    authDomain: "sion-devcat-firebase.firebaseapp.com",
    databaseURL: "https://sion-devcat-firebase.firebaseio.com",
    projectId: "sion-devcat-firebase",
    storageBucket: "sion-devcat-firebase.appspot.com",
    messagingSenderId: "53047941804"
  };
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function(user) {
    if (!user) {
        window.location.href = "/platform/login";
    } else {
        $(".init").css("visibility", "visible");
    }
});